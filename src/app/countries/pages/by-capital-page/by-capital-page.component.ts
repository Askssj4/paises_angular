import { Component, ElementRef, Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Country } from '../../interfaces/country';
import { CountriesService } from '../../services/countries.service';

@Component({
  selector: 'countries-by-capital-page',
  templateUrl: './by-capital-page.component.html',
})
export class ByCapitalPageComponent {

  constructor(private countryService: CountriesService){

  }


  ngOnInit(): void {
    //Called after the constructor, initializing input properties, and the first call to ngOnChanges.
    //Add 'implements OnInit' to the class.
    this.countries = this.countryService.cacheStore.byCapital.countries;
    this.initialValue = this.countryService.cacheStore.byCapital.term
  }

  public countries: Country[]=[];

  public taginput!: ElementRef<HTMLInputElement>;

  public isLoading: boolean = false;

  public initialValue: string = "";


  searchByCapital(value: string){
    console.log({value});
    console.log("Desde capital page");

    this.isLoading = true;

    this.countryService.searchCapital(value).subscribe(resp=>{
      console.log(resp);
      this.countries = resp
      this.isLoading = false
  });



  }

  // constructor(private http: HttpClient){

  // }

  

}
