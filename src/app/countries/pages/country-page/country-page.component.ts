import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CountriesService } from '../../services/countries.service';
import { switchMap } from 'rxjs';
import { Country } from '../../interfaces/country';

@Component({
  selector: 'app-country-page',
  templateUrl: './country-page.component.html',
})
export class CountryPageComponent implements OnInit{

  public country?: Country;

  constructor(private activatedRoute: ActivatedRoute, private countriesServices: CountriesService, private router: Router){}
  ngOnInit(): void {
    this.activatedRoute.params
    .pipe(
      //Recibe el valor anterior y retorna un nuevo observable
      switchMap(
        ({id})=>this.countriesServices.searchById(id)
      )
    )
    .subscribe(
      (resp)=>{
        console.log(resp);
        if(!resp)
        {
          return this.router.navigateByUrl('');
        }
       
         return this.country=resp;
      }
    );
  }

}
