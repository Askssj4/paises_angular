import { Component, ElementRef } from '@angular/core';
import { CountriesService } from '../../services/countries.service';
import { Country } from '../../interfaces/country';

@Component({
  selector: 'app-by-country-page',
  templateUrl: './by-country-page.component.html',
})
export class ByCountryPageComponent {
  constructor(private countryService: CountriesService){

  }

  ngOnInit(): void {
    //Called after the constructor, initializing input properties, and the first call to ngOnChanges.
    //Add 'implements OnInit' to the class.
    this.countries = this.countryService.cacheStore.byCountries.countries;
    this.initialValue = this.countryService.cacheStore.byCountries.term
  }

  public countries: Country[]=[];

  public initialValue: string = '';

  public taginput!: ElementRef<HTMLInputElement>;


  searchByCountry(value: string){
    console.log({value});
    console.log("Desde capital page");

    this.countryService.searchCountry(value).subscribe(resp=>{
      console.log(resp);
      this.countries = resp
  });



  }
}
