import { Component, ElementRef } from '@angular/core';
import { CountriesService } from '../../services/countries.service';
import { Country } from '../../interfaces/country';
import { Region } from '../../interfaces/region.type';



@Component({
  selector: 'app-by-region-page',
  templateUrl: './by-region-page.component.html',
})
export class ByRegionPageComponent {
  constructor(private countryService: CountriesService){

  }

  ngOnInit(): void {
    //Called after the constructor, initializing input properties, and the first call to ngOnChanges.
    //Add 'implements OnInit' to the class.
    this.countries = this.countryService.cacheStore.byRegion.countries;
    this.selectRegion = this.countryService.cacheStore.byRegion.region
  }


  public countries: Country[]=[];

  public taginput!: ElementRef<HTMLInputElement>;

  public initialValue: string = '';

  public regiones: Region[] = ["Africa", "Americas", "Asia", "Europe", "Oceania"]

  public selectRegion?: Region;


  searchByRegion(value: Region){

    this.selectRegion = value;

    console.log({value});
    console.log("Desde capital page");

    this.countryService.searchRegion(value).subscribe(resp=>{
      console.log(resp);
      this.countries = resp
  });

}}
