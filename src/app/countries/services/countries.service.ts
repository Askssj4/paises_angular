import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, tap, map, catchError, of, delay } from 'rxjs';
import { Country } from '../interfaces/country';
import { CacheStore } from '../interfaces/cache-store.interfaces';
import { Region } from '../interfaces/region.type';

//Esto no es un componente de angular, por lo que las cosas se pueden ejecutar en el constructor
@Injectable({providedIn: 'root'})
export class CountriesService {
    constructor(private http:HttpClient) { 
        this.loadFromLocalStorage()
     }
    
    private apiUrl: string = 'https://restcountries.com/v3.1'

    public cacheStore: CacheStore = {
        byCapital: {term: '', countries: []},
        byCountries: {term: '', countries: []},
        byRegion: {region: '', countries: []}
    }

    private saveToLocalStorage(){
        localStorage.setItem('cachestore', JSON.stringify(this.cacheStore) )
    }

    private loadFromLocalStorage(){
        if(localStorage.getItem('cachestore'))
        {
            this.cacheStore = JSON.parse(localStorage.getItem('cachestore')!)
        }
    }


    private getCountriewsRequest(url: string):Observable<Country[]>{
        return this.http.get<Country[]>(url)
        .pipe(
            catchError(()=>of([])),
            //espera de tiempo de 2 segundos
            delay(2000)
        )
    }

    searchCapital(term:string): Observable<Country[]>{

        const url = `${this.apiUrl}/capital/${term}`

        return this.getCountriewsRequest(url)
        .pipe(
            tap(
                countries=>(this.cacheStore.byCapital = {term, countries})
            ),
            tap(
                ()=>{
                    this.saveToLocalStorage()
                }
            )
        );
        
    }

    searchCountry(term:string): Observable<Country[]>{
        
        const url = `${this.apiUrl}/name/${term}`

        return this.getCountriewsRequest(url)
        .pipe(
            tap(
                countries=>this.cacheStore.byCountries = {term, countries}
            ),
            tap(
                ()=>{
                    this.saveToLocalStorage()
                }
            )
        )
        
    }

    searchRegion(term:Region): Observable<Country[]>{
        
        const url = `${this.apiUrl}/region/${term}`

        return this.getCountriewsRequest(url)
        .pipe(
            tap(
                countries=>this.cacheStore.byRegion={region: term, countries}
            ),
            tap(
                ()=>{
                    this.saveToLocalStorage()
                }
            )
        )
        
    }

    searchById(term:string): Observable<Country | null>{
        return this.http.get<Country[]>(`${this.apiUrl}/alpha/${term}`)
        .pipe(
            map(countries=> countries.length>0?countries[0]:null),
            catchError(error=>{
                return of(null)
            })

            
        )
        
    }

    // this.http.get<Country>('https://restcountries.com/v3.1/capital/'+value)
    // .subscribe(resp=>{
    //   console.log(resp);
      
    // })
}