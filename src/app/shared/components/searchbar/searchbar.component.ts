import { Component, EventEmitter, Input, OnDestroy, OnInit, Output } from '@angular/core';
import { Subject, Subscription, debounceTime } from 'rxjs';

@Component({
  selector: 'shared-searchbar',
  templateUrl: './searchbar.component.html',
})
export class SearchbarComponent implements OnInit, OnDestroy{
  

  private debouncer: Subject<string> = new Subject<string>();

  //tipo suscripcion
  private debouncerSuscription?: Subscription;

  ngOnInit(): void {
    this.debouncerSuscription =  this.debouncer
    .pipe(
      debounceTime(2000)
    )
    .subscribe(value=>{
      console.log('debouncer value', value);
      this.onDebounce.emit(value)
    })
  }
  //Destruye las suscripciones de los eventos, como eliminar los useEffects de los componentes
  ngOnDestroy(): void {

    //desuscribe
    this.debouncerSuscription?.unsubscribe()
    
  }

  @Input()
  public placeholder:  string= '';

  @Output()
  public onValue = new EventEmitter<string>()

  @Output()
  public onDebounce = new EventEmitter<string>()

  @Input()
  public initialValue: string = ""

  emitValue(value: string){
    this.onValue.emit(value)
  }

  onKeyPress(searchTerm: string){
    console.log(searchTerm);
    //Hacer una siguiente emisión del debouncer
    this.debouncer.next(searchTerm);
    
  }
}
