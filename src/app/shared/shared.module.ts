import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomePageComponent } from './pages/home-page/home-page.component';
import { AboutPageComponent } from './pages/about-page/about-page.component';
import { SidebarComponent } from './components/sidebar/sidebar.component';
import { RouterModule } from '@angular/router';
import { ContactPageComponent } from './pages/contact-page/contact-page.component';
import { SearchbarComponent } from './components/searchbar/searchbar.component';
import { LoadingSpinnerComponent } from './components/loading-spinner/loading-spinner.component';



@NgModule({
  declarations: [
    HomePageComponent,
    AboutPageComponent,
    SidebarComponent,
    ContactPageComponent,
    SearchbarComponent,
    LoadingSpinnerComponent
  ],
  imports: [
    CommonModule,
    //Esto sirve para que los elementos HTML utilicen todo lo relacionado al Router
    RouterModule
  ],
  exports: [HomePageComponent, AboutPageComponent, SidebarComponent, ContactPageComponent, SearchbarComponent, LoadingSpinnerComponent]
})
export class SharedModule { }
